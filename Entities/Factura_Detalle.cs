namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Factura-Detalle")]
    public partial class Factura_Detalle
    {

        [Key]
        [Column(Order = 1)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idFactura { get; set; }

        [Key]
        [Column(Order = 2)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idDetalle { get; set; }

        //public virtual Detalle Detalle { get; set; }

        //public virtual Factura Factura { get; set; }
    }
}
