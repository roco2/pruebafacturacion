namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Producto")]
    public partial class Producto
    {
        public int id { get; set; }

        [Required(ErrorMessage = "campo nombre requerido")]
        [StringLength(50)]
        public string nombre { get; set; }

        [Required(ErrorMessage = "campo descripcion requerido")]
        [StringLength(100)]
        public string descripcion { get; set; }

        [Required(ErrorMessage = "campo precio requerido")]
        public int precio { get; set; }
    }
}
