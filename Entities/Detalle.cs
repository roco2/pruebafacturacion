namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Detalle")]
    public partial class Detalle
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public Detalle()
        //{
        //    Factura_Detalle = new HashSet<Factura_Detalle>();
        //}

        public int id { get; set; }

        [Required]
        [StringLength(100)]
        public string producto { get; set; }

        [Column("detalle")]
        [Required]
        [StringLength(100)]
        public string detalle1 { get; set; }

        public int cantidad { get; set; }
        
        public string numerofact { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Factura_Detalle> Factura_Detalle { get; set; }
    }
}
