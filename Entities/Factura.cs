namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Factura")]
    public partial class Factura
    {
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        //public Factura()
        //{
        //    Factura_Detalle = new HashSet<Factura_Detalle>();
        //}

        public int id { get; set; }

        [Required]
        public string numero { get; set; }

        public int idcliente { get; set; }

        [Column(TypeName = "date")]
        public DateTime fechaexpedicion { get; set; }

        //public virtual Cliente Cliente { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Factura_Detalle> Factura_Detalle { get; set; }
    }
}
