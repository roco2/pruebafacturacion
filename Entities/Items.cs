﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Items
    {
        public int id { get; set; }
        public string producto { get; set; }
        public string detalle { get; set; }
        public string cantidad { get; set; }
        public int cliente { get; set; }
        public string precio { get; set; }
    }
}
