﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Response
    {
        public bool estado { get; set; } = true;
        public string mensaje { get; set; }

        public Exception Exception { get; set; } = new Exception();

    }
}
