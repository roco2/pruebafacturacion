﻿using DataAccess;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic
{
    public class BussinesFactura
    {
        private context ctx = new context();

        public List<Factura> GetAll()
        {
            return ctx.Facturas.ToList();
        }

        public Factura GetById(int Id)
        {
            return ctx.Facturas.Where(p => p.id == Id).FirstOrDefault();
        }

        public Response Insert(List<Items> items)
        {
            try
            {
                Factura factura = new Factura();
                factura.idcliente = items.Select(i=>i.cliente).FirstOrDefault();
                factura.fechaexpedicion = DateTime.Now;
                factura.numero = Guid.NewGuid().ToString();
                ctx.Facturas.Add(factura);
                ctx.SaveChanges();

                foreach (var item in items)
                {
                    Detalle detalle = new Detalle();
                    detalle.producto = item.id.ToString();
                    detalle.cantidad = int.Parse(item.cantidad);
                    detalle.detalle1 = item.detalle + ' ' + item.producto ;
                    detalle.numerofact = factura.numero;
                    ctx.Detalles.Add(detalle);
                    ctx.SaveChanges();

                    Factura_Detalle facturaDetalle= new Factura_Detalle();
                    facturaDetalle.idFactura = ctx.Facturas.Where(f=>f.numero==factura.numero).FirstOrDefault().id;
                    facturaDetalle.idDetalle = ctx.Detalles.Where(f => f.numerofact == factura.numero).FirstOrDefault().id;
                    var fd = ctx.Factura_Detalle.Add(facturaDetalle);
                    ctx.SaveChanges();
                }

                return new Response() { 
                    mensaje="Correcto"
                };
            }
            catch (Exception e)
            {
                return new Response()
                {
                    estado = false,
                    mensaje = e.Message,
                    Exception = e
                };
            }
        }

        //public bool Update(Factura factura)
        //{
        //    try
        //    {
        //        ctx.Facturas.Attach(factura);

        //        ctx.Entry(factura).Property(a => a.).IsModified = true;
        //        ctx.Entry(factura).Property(a => a.).IsModified = true;
        //        ctx.Entry(factura).Property(a => a.).IsModified = true;
        //        ctx.Entry(factura).Property(a => a.).IsModified = true;
        //        ctx.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }
        //}

        public bool Remove(int Id)
        {
            try
            {
                Factura factura = ctx.Facturas.Find(Id);
                ctx.Facturas.Remove(factura);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
