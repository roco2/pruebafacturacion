﻿using DataAccess;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinesLogic
{
    public class BussinesCliente
    {
        private context ctx = new context();

        public List<Cliente> GetAll()
        {
            return ctx.Clientes.ToList();
        }

        public Cliente GetById(int Id)
        {
            return ctx.Clientes.Where(p => p.id == Id).FirstOrDefault();
        }

        public bool Insert(Cliente cliente)
        {
            try
            {
                ctx.Clientes.Add(cliente);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Update(Cliente cliente)
        {
            try
            {
                ctx.Clientes.Attach(cliente);

                ctx.Entry(cliente).Property(a => a.nombre).IsModified = true;
                ctx.Entry(cliente).Property(a => a.apellido).IsModified = true;
                ctx.Entry(cliente).Property(a => a.documento).IsModified = true;
                ctx.Entry(cliente).Property(a => a.telefono).IsModified = true;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Remove(int Id)
        {
            try
            {
                Cliente cliente = ctx.Clientes.Find(Id);
                ctx.Clientes.Remove(cliente);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
