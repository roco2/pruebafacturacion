﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using Entities;

namespace BussinesLogic
{
    public class BussinesProducto
    {
        private context ctx = new context();

        public List<Producto> GetAll()
        {
            return ctx.Productos.ToList();
        }

        public Producto GetById(int Id)
        {
            return ctx.Productos.Where(p => p.id == Id).FirstOrDefault();
        }

        public bool Insert(Producto producto)
        {
            try
            {
                ctx.Productos.Add(producto);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Update(Producto producto)
        {
            try
            {
                ctx.Productos.Attach(producto);

                ctx.Entry(producto).Property(a => a.nombre).IsModified = true;
                ctx.Entry(producto).Property(a => a.precio).IsModified = true;
                ctx.Entry(producto).Property(a => a.descripcion).IsModified = true;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Remove(int Id)
        {
            try
            {
                Producto producto = ctx.Productos.Find(Id);
                ctx.Productos.Remove(producto);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
