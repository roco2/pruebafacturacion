﻿using BussinesLogic;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ApiRest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Route("api/factura")]
    public class FacturaController : ApiController
    {
        private BussinesFactura bussinesFactura = new BussinesFactura();

        [HttpGet]
        public IEnumerable<Factura> Get()
        {
            var lista = bussinesFactura.GetAll().OrderBy(d => d.idcliente).ThenBy(d => d.id);
            return lista;
        }

        [Route("api/factura/{id}")]
        [HttpGet()]
        public IHttpActionResult Get(int id)
        {
            var factura = bussinesFactura.GetById(id);
            return Ok(factura);
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]List<Items> items)
        {
            if (ModelState.IsValid)
            {
                return Ok(bussinesFactura.Insert(items));
            }
            return Ok("Valide la estrucutura del Objeto");
        }

        [HttpPatch]
        public IHttpActionResult Patch([FromBody]Factura factura)
        {
            if (ModelState.IsValid)
            {
                if (true)//(bussinesFactura.Update(factura))
                    return Ok("Actualización Correcta");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }

        [Route("api/factura/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                if (bussinesFactura.Remove(id))
                    return Ok("Registro eliminado");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }
    }
}
