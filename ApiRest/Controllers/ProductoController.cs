﻿using BussinesLogic;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ApiRest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Route("api/producto")]
    public class ProductoController : ApiController
    {
        private BussinesProducto bussinesProducto = new BussinesProducto();

        [HttpGet]
        public IHttpActionResult Get()
        {
            var lista = bussinesProducto.GetAll();
            return Ok(lista);
        }

        [Route("api/producto/{id}")]
        [HttpGet()]
        public IHttpActionResult Get(int id)
        {
            var producto = bussinesProducto.GetById(id);
            return Ok(producto);
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]Producto producto)
        {
            if (ModelState.IsValid)
            {
                if (bussinesProducto.Insert(producto))
                    return Ok("Grabación Correcta");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }

        [HttpPatch]
        public IHttpActionResult Patch([FromBody]Producto producto)
        {
            if (ModelState.IsValid)
            {
                if (bussinesProducto.Update(producto))
                    return Ok("Actualización Correcta");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }

        [Route("api/producto/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                if (bussinesProducto.Remove(id))
                    return Ok("Registro eliminado");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }
    }
}
