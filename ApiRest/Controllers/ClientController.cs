﻿using BussinesLogic;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ApiRest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Route("api/cliente")]
    public class ClientController : ApiController
    {
        private BussinesCliente bussinesCliente = new BussinesCliente();

        [HttpGet]
        public IEnumerable<Cliente> Get()
        {
            var lista = bussinesCliente.GetAll().OrderBy(d=> d.nombre).ThenBy(d=>d.apellido);
            return lista;
        }

        [Route("api/cliente/{id}")]
        [HttpGet()]
        public IHttpActionResult Get(int id)
        {
            var cliente = bussinesCliente.GetById(id);
            return Ok(cliente);
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                if (bussinesCliente.Insert(cliente))
                    return Ok("Grabación Correcta");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }

        [HttpPatch]
        public IHttpActionResult Patch([FromBody]Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                if (bussinesCliente.Update(cliente))
                    return Ok("Actualización Correcta");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }

        [Route("api/cliente/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                if (bussinesCliente.Remove(id))
                    return Ok("Registro eliminado");
                else
                    return Ok("Ocurrió un error");
            }
            return Ok("Valide la estrucutura del Objeto");
        }
    }
}
