namespace DataAccess
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Entities;

    public partial class context : DbContext
    {
        public context()
            : base("name=context")
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Detalle> Detalles { get; set; }
        public virtual DbSet<Factura> Facturas { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Factura_Detalle> Factura_Detalle { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.apellido)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.documento)
                .IsUnicode(false);

            modelBuilder.Entity<Cliente>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            //modelBuilder.Entity<Cliente>()
            //    .HasMany(e => e.Facturas)
            //    .WithRequired(e => e.Cliente)
            //    .HasForeignKey(e => e.idcliente)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Detalle>()
                .Property(e => e.producto)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle>()
                .Property(e => e.detalle1)
                .IsUnicode(false);

            modelBuilder.Entity<Detalle>()
                .Property(e => e.numerofact)
                .IsUnicode(false);

            //modelBuilder.Entity<Detalle>()
            //    .HasMany(e => e.Factura_Detalle)
            //    .WithRequired(e => e.Detalle)
            //    .HasForeignKey(e => e.idDetalle)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.numero)
                .IsUnicode(false);

            //modelBuilder.Entity<Factura>()
            //    .HasMany(e => e.Factura_Detalle)
            //    .WithRequired(e => e.Factura)
            //    .HasForeignKey(e => e.idFactura)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Producto>()
                .Property(e => e.descripcion)
                .IsUnicode(false);
        }
    }
}
